import instaloader

def get_follower_count(username):
    try:
        L = instaloader.Instaloader()
        profile = instaloader.Profile.from_username(L.context, username)
        follower_count = profile.followers
        return follower_count
    except Exception as e:
        print(f"An error occurred: {e}")
        return None

if __name__ == "__main__":
    username = "r1rider_"  # Replace with your Instagram username
    follower_count = get_follower_count(username)
    
    if follower_count is not None:
        print(f"Your follower count is: {follower_count}")
